Rails.application.routes.draw do
  get 'wlcome/index'

  resources :articles do 
    resources :comments
  end
  
  root 'wlcome#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
